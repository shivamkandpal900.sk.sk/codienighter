<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Controller extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->database();
    $this->load->helper(array('form'));
    $this->load->library('form_validation');
    $this->load->model('Registerfetch');
    $this->load->library('session');

  }

  public function index()
  {
      $this->load->view('theme/header');
      $this->load->view('register/login');
      $this->load->view('theme/footer');
  }
//admin_dashboard
  public function login() {
    $username = $this->input->post('uname');
    $password = $this->input->post('pass');
    if($this->Registerfetch->loginuser($username,$password))
    {
      $session_data = array(
        'username' => $username
      );
      $this->session->set_userdata($session_data);
      redirect(base_url(). 'admin_dashboard');
    }
    else{ $this->session->set_flashdata('error','Invalid Username and Password');
      redirect(base_url() . 'Login_Controller');
    }
  }


  public function admin_dashboard()
  {
    // code...
    $this->load->view('theme/header');
    $this->load->view('register/admin_dash');
    $this->load->view('theme/footer');

  $list = $this->admit_list->table->($firstname,$username, $lastname,$password);
  $data['rows'] = $list;
  }
}
