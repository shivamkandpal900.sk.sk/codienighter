<?php
session_start();
// Destroy all sessions
if(session_destroy())
{
  // Redirection To Home page
  header("Location: login.php");
}
 ?>
