<html>
   <head>
      <title>The Materialize Example</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">
      <link rel = "stylesheet"
         href = "https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel = "stylesheet"
         href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
      <script type = "text/javascript"
         src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
      </script>
   </head>

   <body class="container">
     <div class="row">
    <nav>
   <div class="nav-wrapper">
     <a href="#!" class="brand-logo"><i class="material-icons">cloud</i>MindMineLabs</a>
     <ul class="right hide-on-med-and-down">
       <li><a href="sass.html"><i class="material-icons">search</i></a></li>
       <li><a href="badges.html"><i class="material-icons">view_module</i></a></li>
       <li><a href="collapsible.html"><i class="material-icons">refresh</i></a></li>
       <li><a href="mobile.html"><i class="material-icons">more_vert</i></a></li>
     </ul>
   </div>
 </nav>
 <br>
