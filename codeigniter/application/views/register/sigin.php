<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

      <div class="row">
      <form method="post" action="<?php echo base_url("Register/insert");?>"

        <div class="input-field col s6">
          <input placeholder="First Name" name= 'fname' id="fname" type="text" class="validate">
          <label for="first_name"></label>
        </div>

        <div class="input-field col s6">
          <input placeholder="Last Name" name='lname' id="lname" type="text" class="validate">
          <label for="last_name"></label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s6">
          <input placeholder="User Name" name = 'uname' id="uname" type="text" class="validate">
          <label for="user_name"></label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Email address" name ='email' id="email" type="text" class="validate">
          <label for="email"></label>
        </div>
        <div class="input-field col s6">
          <input placeholder="Mobile No" id="mobno" type="text" class="validate">
          <label for="Mobile_NO"></label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Password" name='pass' id="pass" type="text" class="validate">
          <label for="password"></label>
        </div>
        <div class="input-field col s6">
          <input placeholder="Confirm Password" id="cpass" type="text" class="validate">
          <label for="confirm_password"></label>
        </div>
      </div>

      <div class="row">
        <div class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input id="add" type="text" class="validate" placeholder="Address">
          <label for="Address"></label>
        </div>
        <div class="input-field col s12">
          <input id="cadd" type="text" class="validate" placeholder="Correspondence_Address">
          <label for="Correspondence_Address"></label>
        </div>
      </div>

      <button class="btn waves-effect waves-light" type="submit" name="insert">Submit
    <i class="material-icons right">send</i>
  </button>
  </div>
  </div>
</form>
